using System;
using System.Collections.Immutable;
using NUnit.Framework;

namespace Deathrage.Compounder.Tests
{
    public class Tests
    {
        private class Foo
        {
            public string Do() => "foo";

            public string DoGeneric<TArg>() => typeof(TArg).Name;
        }

        private class Bar
        {
            public int Stuff() => 8;

            public string StuffGeneric<TArg>() => typeof(TArg).Name;
        }

        [Test]
        public void DefaultCompositionExecutor_Execute()
        {
            DefaultCompositionExecutor sut = new DefaultCompositionExecutor();

            ExecutionQueue queue = new ExecutionQueue(new object[0], new Type[0], 2);

            Foo foo = new Foo();
            Bar bar = new Bar();

            queue.Enqueue(new ExecutionItem(foo, foo.GetType().GetMethod(nameof(Foo.Do))));
            queue.Enqueue(new ExecutionItem(bar, bar.GetType().GetMethod(nameof(Bar.Stuff))));
                
            ImmutableArray<object> results = sut.Execute(queue).ToImmutableArray();

            CollectionAssert.AreEqual(new object[] {"foo", 8}, results);
        }

        [Test]
        public void DefaultCompositionExecutor_Execute_Generic()
        {
            DefaultCompositionExecutor sut = new DefaultCompositionExecutor();

            ExecutionQueue queue = new ExecutionQueue(new object[0], new Type[] { typeof(string) }, 2);

            Foo foo = new Foo();
            Bar bar = new Bar();

            queue.Enqueue(new ExecutionItem(foo, foo.GetType().GetMethod(nameof(Foo.DoGeneric))));
            queue.Enqueue(new ExecutionItem(bar, bar.GetType().GetMethod(nameof(Bar.StuffGeneric))));
                
            ImmutableArray<object> results = sut.Execute(queue).ToImmutableArray();

            CollectionAssert.AreEqual(new object[] {"String", "String"}, results);
        }

        [Test]
        public void DefaultCompositionExecutor_Execute_NonGenericWithGenericParams()
        {
            DefaultCompositionExecutor sut = new DefaultCompositionExecutor();

            ExecutionQueue queue = new ExecutionQueue(new object[0], new Type[] { typeof(string) }, 2);

            Foo foo = new Foo();
            Bar bar = new Bar();
            
            queue.Enqueue(new ExecutionItem(foo, foo.GetType().GetMethod(nameof(Foo.Do))));
            queue.Enqueue(new ExecutionItem(bar, bar.GetType().GetMethod(nameof(Bar.Stuff))));

            Assert.Throws<InvalidOperationException>(() => sut.Execute(queue));
        }

        [Test]
        public void DefaultCompositionExecutor_Execute_GenericWithoutParameters()
        {
            DefaultCompositionExecutor sut = new DefaultCompositionExecutor();

            ExecutionQueue queue = new ExecutionQueue(new object[0], new Type[0], 2);

            Foo foo = new Foo();
            Bar bar = new Bar();

            queue.Enqueue(new ExecutionItem(foo, foo.GetType().GetMethod(nameof(Foo.DoGeneric))));
            queue.Enqueue(new ExecutionItem(bar, bar.GetType().GetMethod(nameof(Bar.StuffGeneric))));
                
            Assert.Throws<InvalidOperationException>(() => sut.Execute(queue));
        }

        [Test]
        public void DefaultCompositionExecutor_Execute_GenericTooManyParameters()
        {
            DefaultCompositionExecutor sut = new DefaultCompositionExecutor();

            ExecutionQueue queue = new ExecutionQueue(new object[0], new Type[] { typeof(int), typeof(string)}, 2);

            Foo foo = new Foo();
            Bar bar = new Bar();

            queue.Enqueue(new ExecutionItem(foo, foo.GetType().GetMethod(nameof(Foo.DoGeneric))));
            queue.Enqueue(new ExecutionItem(bar, bar.GetType().GetMethod(nameof(Bar.StuffGeneric))));
                
            Assert.Throws<ArgumentException>(() => sut.Execute(queue));
        }
    }
}