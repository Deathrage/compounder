using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// In SDK-style projects such as this one, several assembly attributes that were historically
// defined in this file are now automatically added during build and populated with
// values defined in project properties. For details of which attributes are included
// and how to customise this process see: https://aka.ms/assembly-info-properties


// Setting ComVisible to false makes the types in this assembly not visible to COM
// components.  If you need to access a type in this assembly from COM, set the ComVisible
// attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM.

[assembly: Guid("3fd4281b-a3d4-4196-bb81-5bdeb38496f9")]

[assembly: InternalsVisibleTo("Deathrage.Compounder.Tests, PublicKey=00240000048000009400000006020000002400005253413100040000010001001d4fa1930dc824b5cd2053ebfe3af9a8d80298225838ddd793e335c0bf152deaf1608d45e08d6254084e1024dfd2c590df3bd204e7526d232f421e30aa340bc5f75f1d149e7e4f53ef2f3aae57d5a4711de1d88297f21ee0be93af19e7a9c07b479da26791e28af9ff423a2c0467ec300ed3f52a021111077b0d9aa013c840c8")]
