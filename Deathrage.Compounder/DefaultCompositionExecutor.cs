﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Deathrage.Compounder.Extensions;

namespace Deathrage.Compounder
{
    internal class DefaultCompositionExecutor : ICompositionExecutor
    {
        public IEnumerable Execute(ExecutionQueue queue)
        {
            List<object> results = new List<object>(queue.Count);

            foreach (ExecutionItem executionItem in queue)
            {
                MethodInfo executionMethod = queue.IsGenericDefinition
                    ? executionItem.ExecutionMethod.EnforceGenericDefinition()
                        .MakeGenericMethod(queue.GenericParameters)
                    : executionItem.ExecutionMethod;

                object result = executionMethod.Invoke(executionItem.MemberInstance, queue.Parameters);
                results.Add(result);
            }

            return results;
        }
    }
}