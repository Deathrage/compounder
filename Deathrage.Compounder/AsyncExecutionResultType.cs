﻿namespace Deathrage.Compounder
{
    public enum AsyncExecutionResultType
    {
        Task,
        ValueTask
    }
}