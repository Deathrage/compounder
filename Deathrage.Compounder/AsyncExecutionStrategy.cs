﻿namespace Deathrage.Compounder
{
    public enum AsyncExecutionStrategy
    {
        Parallel,
        Serial,
        Race
    }
}