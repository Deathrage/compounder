﻿using System.Collections;

namespace Deathrage.Compounder
{
    public interface ICompositionExecutor
    {
        IEnumerable Execute(ExecutionQueue queue);
    }
}