﻿using System.Collections;
using System.Threading.Tasks;

namespace Deathrage.Compounder
{
    public interface ICompositionAsyncExecutor
    {
        ValueTask<IEnumerable> Execute(AsyncExecutionQueue queue, AsyncExecutionStrategy strategy);
    }
}