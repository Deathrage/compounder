﻿using System;
using System.Collections.Generic;

namespace Deathrage.Compounder
{
    public class ExecutionQueue: Queue<ExecutionItem>
    {
        public object[] Parameters { get; }
        public Type[] GenericParameters { get; }
        public bool IsGenericDefinition => GenericParameters?.Length > 0;

        public ExecutionQueue(object[] parameters, Type[] genericParameters, int capacity) : base(capacity)
        {
            Parameters = parameters ?? throw new ArgumentNullException(nameof(parameters));
            GenericParameters = genericParameters ?? throw new ArgumentNullException(nameof(parameters));
        }
    }
}