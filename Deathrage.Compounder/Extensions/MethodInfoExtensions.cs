﻿using System;
using System.Reflection;

namespace Deathrage.Compounder.Extensions
{
    internal static class MethodInfoExtensions
    {
        internal static MethodInfo EnforceGenericDefinition(this MethodInfo method)
        {
            if (!method.IsGenericMethodDefinition)
                throw new InvalidOperationException($"Method {method.DeclaringType.FullName}.{method.Name} is not a generic definition");

            return method;
        } 
    }
}