﻿using System;

namespace Deathrage.Compounder
{
    public class AsyncExecutionQueue : ExecutionQueue
    {
        public AsyncExecutionResultType ResultType { get; }

        public AsyncExecutionQueue(object[] parameters, Type[] genericParameters, AsyncExecutionResultType resultType,
            int capacity) : base(parameters, genericParameters, capacity)
        {
            ResultType = resultType;
        }
    }
}