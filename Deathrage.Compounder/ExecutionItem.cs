﻿using System;
using System.Reflection;

namespace Deathrage.Compounder
{
    public class ExecutionItem
    {
        public object MemberInstance { get; }
        public MethodInfo ExecutionMethod { get; }

        public ExecutionItem(object memberInstance, MethodInfo executionMethod)
        {
            MemberInstance = memberInstance ?? throw new ArgumentNullException(nameof(memberInstance));
            ExecutionMethod = executionMethod ?? throw new ArgumentNullException(nameof(executionMethod));
        }
    }
}