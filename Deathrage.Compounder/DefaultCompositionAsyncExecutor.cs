﻿using System;
using Deathrage.Compounder.Extensions;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace Deathrage.Compounder
{
    public class DefaultCompositionAsyncExecutor: ICompositionAsyncExecutor
    {
        public ValueTask<IEnumerable> Execute(AsyncExecutionQueue queue, AsyncExecutionStrategy strategy)
        {
            switch (strategy)
            {
                case AsyncExecutionStrategy.Serial: return ExecuteSerial(queue);
                case AsyncExecutionStrategy.Parallel: return ExecuteParallel(queue);
                case AsyncExecutionStrategy.Race: return ExecuteRace(queue);
            }
        }

        private ValueTask<IEnumerable> ExecuteParallel(AsyncExecutionQueue queue)
        {
            List<object> results = new List<object>(queue.Count);

            foreach (ExecutionItem executionItem in queue)
            {
                MethodInfo executionMethod = queue.IsGenericDefinition
                    ? executionItem.ExecutionMethod.EnforceGenericDefinition()
                        .MakeGenericMethod(queue.GenericParameters)
                    : executionItem.ExecutionMethod;

                object result = executionMethod.Invoke(executionItem.MemberInstance, queue.Parameters);
                results.Add(result);
            }

            return new ValueTask<IEnumerable>(results);
        }

        private ValueTask<IEnumerable> ExecuteRace(AsyncExecutionQueue queue)
        {

        }

        private async ValueTask<IEnumerable> ExecuteSerial(AsyncExecutionQueue queue)
        {
            List<object> results = new List<object>(queue.Count);

            foreach (ExecutionItem executionItem in queue)
            {
                MethodInfo executionMethod = queue.IsGenericDefinition
                    ? executionItem.ExecutionMethod.EnforceGenericDefinition()
                        .MakeGenericMethod(queue.GenericParameters)
                    : executionItem.ExecutionMethod;

                object result = executionMethod.Invoke(executionItem.MemberInstance, queue.Parameters);
                results.Add(result);

                Task? awaitableTask = null;
                if (queue.ResultType == AsyncExecutionResultType.Task)
                {
                    awaitableTask = result as Task;
                    if (awaitableTask is null)
                        ThrowInvalidResultType($"{executionMethod.DeclaringType.FullName}.{executionMethod.Name}",
                            AsyncExecutionResultType.Task, result.GetType().FullName);
                }

                if (queue.ResultType == AsyncExecutionResultType.ValueTask)
                {
                    Type resultType = result.GetType();
                    if (!resultType.IsGenericType || resultType.GetGenericTypeDefinition() != typeof(ValueTask<>))
                        ThrowInvalidResultType($"{executionMethod.DeclaringType.FullName}.{executionMethod.Name}",
                            AsyncExecutionResultType.ValueTask, resultType.FullName);

                    MethodInfo asTask = resultType.GetMethod(nameof(ValueTask.AsTask));
                    awaitableTask = asTask.Invoke(result, new object[]{}) as Task;
                }

                if (awaitableTask is null)
                    throw new InvalidOperationException($"{nameof(Task)} {nameof(awaitableTask)} was not assigned");

                await awaitableTask.ConfigureAwait(false);
            }

            return results;
        }

        private void ThrowInvalidResultType(string methodFullName, AsyncExecutionResultType expected, string actual)
            => throw new InvalidOperationException($"Result for method {methodFullName} was {actual} even though {nameof(AsyncExecutionQueue.ResultType)} is set to {expected}");
    }
}